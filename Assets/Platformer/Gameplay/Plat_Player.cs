﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plat_Player : MonoBehaviour
{
    [SerializeField]
    bool advancedJump; //Marcar como true no ispetor para ativar as funcionalidades avançadas da física de pulo (IE. pulo fraco se o botão for apertado rápidamente)
    
    //Variáveis para físicas de movimentação
    public float moveSpeed; //Define velocidade de movimento do personagem
    public float jumpForce; //Define a força do pulo
    public float fallMultiplier; //Valor que acelera a queda do jogador 
    public float lowJumpMultiplier; //Valor que acelera a queda do jogador  durante um pulo fraco

    public int dashDuration; //Tempo de duração do Dash

    int jumpCooldown;
    int direction = 1;
    int lockedDirection = 0;
    bool isMoving;

    float defaultGravity;
    int activeSkill;
    bool onGround;
    bool airAction;
    bool dashing = false;
    bool dead = false;

    /*Variáveis de efeitos sonoros
    public AudioClip seDash;
    public AudioClip seJump;
    public AudioClip seWalk;
    public AudioClip seDeath;*/

    //Referencias a outros componentes
    //GameControl gameControl; //Descomentar se aplicável
    AudioSource soundPlayer;
    Rigidbody2D playerBody;
    Animator playerAnimator;

    //Novo sistema de input
    PlatformerInput inputAction;
    Vector2 inputMove;

    
    void Awake()
    {
        inputAction = new PlatformerInput();
        inputAction.PlayerControls.Move.performed += ctx => inputMove = ctx.ReadValue<Vector2>();


        //gameControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControl>(); //descomentar se aplicável
        soundPlayer = gameObject.GetComponent<AudioSource>();
        playerBody = gameObject.GetComponent<Rigidbody2D>();
        playerAnimator = gameObject.GetComponent<Animator>();
        defaultGravity = playerBody.gravityScale;
        activeSkill = 0;
    }

    void FixedUpdate()
    {
        PlayerMove();
        if (playerBody.velocity.y < -0.6){

            Vector2 fallApply = Vector2.up * Physics2D.gravity.y * (fallMultiplier - defaultGravity) * Time.deltaTime;
            if (fallApply.x < 4 && fallApply.x > -4 && fallApply.y < 4 && fallApply.y > -4){
                playerBody.velocity += fallApply;


            }
        } else if ((advancedJump) && playerBody.velocity.y > 0 && !Input.GetButton("Jump")){
            playerBody.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - defaultGravity) * Time.deltaTime; 
        }

        if (playerBody.velocity.x > 11 || playerBody.velocity.y > 11 ){
            playerBody.velocity = new Vector2(0,0);
        }
    }

    //Método de movimentação de jogador
    void PlayerMove(){
        if (!dashing && !dead){
            //Vetor a ser aplicado no personagem
            Vector2 speedApply = new Vector2(moveSpeed * Time.fixedDeltaTime * inputMove.x,
                                                playerBody.velocity.y);
            
            //Travas de segurança para que o personagem não fique rápido demais
            if((lockedDirection > 0 && speedApply.x > 0) || (lockedDirection < 0 && speedApply.x < 0)){
                speedApply.x = 0;
            }
            if (speedApply.x < 20 && speedApply.x > -20 && speedApply.y < 20 && speedApply.y > -20){
                playerBody.velocity = speedApply;
            }
                
            
            if ((inputMove.y > 0 || inputMove.x > 0.5f) && jumpCooldown == 0){
                if(onGround){
                    Jump();
                /*}else if(activeSkill == 2 && !onGround && !airAction){
                    airAction = true;
                    playerBody.velocity = new Vector2(0,0);
                    Jump();*/
                } 
                jumpCooldown = 15;
            }
            if (Input.GetButtonDown("Fire1")) {
                if (activeSkill == 1){
                    if(onGround || !airAction) {
                        if (!onGround){
                            airAction = true;
                        }
                        playerBody.velocity = new Vector2(0,0);
                        StartCoroutine(Dash());
                    } 
                } else if(activeSkill == 2 && !onGround && !airAction){
                    airAction = true;
                    playerBody.velocity = new Vector2(0,0);
                    Jump();
                }
            }
        }
        if (jumpCooldown > 0){
            jumpCooldown --;
        }
        DirectionControl();
        SpriteWork();
    }

    //Método que define a direção do personagem para o SpriteWork e executa o som de caminhada se o personágem estiver no chão
    void DirectionControl(){
        if (inputMove.x > 0){
                direction = 1;
                setIsMoving(true);
                if(onGround){
                    //soundPlayer.clip = seWalk;
                    //soundPlayer.Play();
                }
            }

            if (inputMove.x < 0){
                direction = -1;
                setIsMoving(true);
                if(onGround){
                    //soundPlayer.clip = seWalk;
                    //soundPlayer.Play();
                }
            }

            if (inputMove.x == 0 && inputMove.y == 0){
                setIsMoving(false);
            }
    }

    //Indica se o personágem está andando para executar a animação
    public bool setIsMoving(bool mov){
        isMoving = mov;
        playerAnimator.SetBool("isMoving", mov);
        return isMoving;
    }

    //Executa o pulo
    void Jump(){
        playerBody.velocity += jumpForce * Vector2.up;
        //soundPlayer.clip = seJump;
        //soundPlayer.Play();
    }

    //Executa o dash
    IEnumerator Dash(){
        //soundPlayer.clip = seDash;
        //soundPlayer.Play();
        dashing = true;
        playerAnimator.SetBool("dashing", dashing);
        playerBody.gravityScale = 0;
        playerBody.position = new Vector2(playerBody.position.x, playerBody.position.y + 0.08f);
        for(int dashTime = 0; dashTime < dashDuration; dashTime++){
            yield return new WaitForFixedUpdate();
            playerBody.velocity = new Vector2((moveSpeed * 3) * Time.fixedDeltaTime * direction ,0);
        }
        playerBody.gravityScale = defaultGravity;
        dashing = false;
        playerAnimator.SetBool("dashing", dashing);
    }

    //Checa colisões
    void OnTriggerEnter2D(Collider2D col){
        if (col.gameObject.tag == "Hazzard"){
            if (!dead){
                //soundPlayer.clip = seDeath;
                //soundPlayer.Play();
                StartCoroutine(Death());
            }
        }

        /*Descomentar se aplicável
        if (col.gameObject.tag == "Exit"){
            gameControl.EndLevel(col.gameObject.GetComponent<Exit>().nextLevelName);
        } */
    }

    //Executa morte do jogador
    IEnumerator Death(){
        dead = true;
        playerBody.velocity = new Vector2(0,0);
        GetComponent<Collider2D>().enabled = false;
        playerBody.velocity += jumpForce * Vector2.up; //Animação de morte, substituir conforme a necessidade
        yield return new WaitForSeconds(1);
        //gameControl.Restart(); //Descomentar se aplicável
    }

    //Método que define se o jogador está no chão
    public void SetOnGround(bool grounded){
        if (grounded){
            onGround = true;
            airAction = false;
            //Aqui pode ser adicionado uma função para reproduzir um efeito sonoro de aterrisagem
        } else{
            onGround = false;
        }
        playerAnimator.SetBool("onGround", onGround);
    }

    //Trabalha com o direcionamento da sprite
    void SpriteWork(){
        Vector3 currentScale = gameObject.transform.localScale;
        if ((direction == 1 && currentScale.x < 0) || (direction == -1 && currentScale.x > 0)){
            gameObject.transform.localScale = new Vector3(currentScale.x * -1,currentScale.y,1);
        }
    }

    //Trava a diração do jogador
    public void LockDirection(){
        lockedDirection = direction;
    }

    //Destrava a diração do jogador
    public void UnlockDirection(){
        lockedDirection = 0;
    }

    //Retorna a direção do jogador
    public int GetDirection(){
        return direction;
    }


    //Métodos para imput de touchscreen (api imput antiga)
    
    //Fazer Dash por um botão do canvas
    public void DashButtom(){
        if(onGround || !airAction) {
            if (!onGround){
                airAction = true;
            }
            playerBody.velocity = new Vector2(0,0);
            StartCoroutine(Dash());
        }
    }
    
    //Fazer Pulo duplo por um botão do canvas
    public void DoubleJumpButtom(){
        if(!onGround && !airAction){
            airAction = true;
            playerBody.velocity = new Vector2(0,0);
            Jump();
        }
    }

    void OnEnable(){
        inputAction.Enable();
    }

    void OnDisable(){
        inputAction.Disable();
    }
}
