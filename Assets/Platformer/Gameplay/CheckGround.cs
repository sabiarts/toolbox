﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    public Plat_Player player;
    public float offsetX;
    public float offsetY;

    void Start(){
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), player.gameObject.GetComponent<Collider2D>());
    }

    void FixedUpdate(){
        transform.position = new Vector2(player.gameObject.transform.position.x  + (offsetX * player.GetDirection()),
                                         player.gameObject.transform.position.y + offsetY);
    }
    
    void OnTriggerEnter2D(Collider2D col){
        if (col.gameObject.tag == "Floor"){
            player.SetOnGround(true);
        }
    }
    
    void OnTriggerExit2D(Collider2D col){
        if (col.gameObject.tag == "Floor"){
            player.SetOnGround(false);
        }
    }
}
