﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    /*/Player player;
    bool paused = false;

    public Transform crystal;
    public Transform[] crystalWaypoints;

    public GameObject pauseMenu;
    public GameObject endLevel;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel")){
            Pause(!paused);
        }
    }

    public void Pause(bool pause){
        pauseMenu.SetActive(pause);
        if (pause){
            Time.timeScale = 0f;
        } else {
            Time.timeScale = 1f;
        }
    }

    public void EndLevel(string nextLevel){
        PlayerPrefs.SetInt(nextLevel, 1);
        ChangeScene(nextLevel);
    }

    public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ChangeScene(string nextScene){
        SceneManager.LoadScene(nextScene);
    }

    public void StopMusic(){
        Destroy(GameObject.FindGameObjectWithTag("Jukebox"));
    }

    public void ActivateJoystic(GameObject joystick){
        joystick.transform.SetAsLastSibling();
    } */
}
