﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevel : MonoBehaviour
{
    public string levelName;
    void Start()
    {
        if (PlayerPrefs.GetInt(levelName, 0) == 0){
            GetComponent<Button>().interactable = false;
        }
    }
}
