﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.AnimationModule;

public class Top_Player : MonoBehaviour
{
    public float moveSpeed;

    public string typeInput;

    public float direction;

    bool isRunning;

    public bool alive;

    string charName;

    //Controller gameManager;

    public Animator anim;

    //Novo sistema de input
    TopDownInput inputAction;
    Vector2 inputMove;

    void Awake()
    {
        inputAction = new TopDownInput();
        inputAction.PlayerControls.Move.performed += ctx => inputMove = ctx.ReadValue<Vector2>();

        alive = true;
        anim = gameObject.GetComponent<Animator>();
        //gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Controller>();
    }

    void OnEnable(){
        inputAction.Enable();
    }

    void OnDisable(){
        inputAction.Disable();
    }

    void Update()
    {
        if (alive ){ //&& !gameManager.paused
            PlayerMove();
            
        } else {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
    
    //Método responsável pela movimentação do personagem
    void PlayerMove(){

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(

                moveSpeed * Time.fixedDeltaTime * inputMove.x,

                moveSpeed * Time.fixedDeltaTime * inputMove.y);
            
        if (inputMove.x < 0){
            
            direction = 1;
            //Debug.Log("->");
            setIsMoving(true);

        }
        if (inputMove.x > 0){
            
            direction = -1;
            //Debug.Log("<-");
            setIsMoving(true);

        }
        if (inputMove.y != 0){
            //Debug.Log("^v");
            setIsMoving(true);
        }
        if (inputMove.x == 0 && inputMove.y == 0){
            //Debug.Log("x");
            setIsMoving(false);
        }
        SpriteWork();
    }

    //Aciona a animação de movimento
    public bool setIsMoving(bool mov){
        isRunning = mov;
        //Debug.Log(mov);
        anim.SetBool("isRunning", mov);

        return isRunning;

    }

    //Script responsável por controlar a direção vertical do Sprite
    void SpriteWork(){
        if (direction == 1){
            if (transform.localScale.x < 0){
                transform.localScale *= new Vector2(-1,1);
                transform.localScale = new Vector3(transform.localScale.x,transform.localScale.y,1);
            }
        }
        if (direction == -1){
            if (transform.localScale.x > 0){
                transform.localScale *= new Vector2(-1,1);
                transform.localScale = new Vector3(transform.localScale.x,transform.localScale.y,1);
            }
        }
    }

    public TopDownInput GetInputAction(){
        return inputAction;
    }

    /*/Todo: Achar nova implementação para esse sistema (aplicavel apenas p/ multiplayer local)
    public string setTypeInput(string type){

        typeInput = type;

        return typeInput;
    }

    public string getTypeInput(){

        return typeInput;
    }*/
}