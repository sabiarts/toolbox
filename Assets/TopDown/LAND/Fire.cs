﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject fireObject; //objeto a ser atirado
    public Top_Player player; //referencia ao objeto que dispara esta ação, alterar conforme necessário
    public Vector2 offset; //distancia na qual o objeto disparado irá aoarecer do disparador
    public float cooldown; //cooldown para realizar o disparo novamente após o anterior

    Collider2D playerCollider; //verificar necessidade de fazer essa busca no Top_Player
    TopDownInput inputAction;

    void Start()
    {
        playerCollider = GetComponent<Collider2D>();
        inputAction = player.GetInputAction();
        inputAction.PlayerControls.Action.performed += ctx => FireAction();

    }

    void FireAction(){
        if (offset.x == 0 && offset.y == 0){
            Collider2D fireCol = Instantiate(fireObject, transform.position, transform.rotation).GetComponent<Collider2D>();
            Physics2D.IgnoreCollision(playerCollider, fireCol); //Remover se o collider do disparo for trigger
        } else{
            Instantiate(
                    fireObject,
                    new Vector2((float)transform.position.x + offset.x, transform.position.y + offset.y),
                    transform.rotation
                );
        }
        
    }
}
